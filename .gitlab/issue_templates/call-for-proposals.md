---
name: Suggest a talk
about: Let's get the meetup started and we always need your talks :)
---

* **Title**:
* **Speaker**:
* **Organization**:
* **Type**: [Presentation 30-45 mins | Lightning talk 5-10 mins | Workshop - 45mins - 2hrs]
* **Level**: [Beginner | Intermediate Advanced]
* **Tags**: [Community, Open-Source, Git, GitLab, Frontend, Backend, UI/UX]


**Description**

*A paragraph describing your proposal*


**Speaker Bio**

*Short Bio of The Speaker*
*Link to Speaker Photo*

* **Twitter**: Your Twitter profile link
* **LinkedIn**: Your LikedIn profile link
* **GitHub**: Your GitLab/GitHub profile link
